package com.iceking.config;

import com.iceking.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by I on 2015/8/14.
 */
@Configuration
@ComponentScan(basePackages = {"com.iceking.model", "com.iceking.controller"})
public class AppConfig {

    @Autowired
    private User user;

    @Bean
    public User userAdmin() {
        user.setName("Kang");
        user.setAge(32);
        return user;
    }

}
