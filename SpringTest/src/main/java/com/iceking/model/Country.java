package com.iceking.model;

import org.springframework.stereotype.Component;

/**
 * Created by I on 2015/8/18.
 */
public class Country {
    private String shortname;
    private String fullname;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getShortname() {

        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public Country(String shortname, String fullname) {
        this.shortname = shortname;
        this.fullname = fullname;
    }
}
