package com.iceking.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by I on 2015/8/16.
 */
@Service
@Qualifier("Cool")
public class CoolService implements DisplayService {
    public String welcome() {
        return "Cool!!!";
    }
}
