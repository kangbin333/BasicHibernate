package com.iceking.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * Created by I on 2015/8/16.
 */
@Service("extraService")
@Primary
public class HelloService implements DisplayService {

    public String welcome() {
        return "Welcome!!!x";
    }

}
