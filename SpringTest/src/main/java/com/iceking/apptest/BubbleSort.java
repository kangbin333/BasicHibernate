package com.iceking.apptest;

/**
 * Created by I on 2015/8/25.
 */
public class BubbleSort {
        public static void main(String[] args) {
            int[] unsort = {2, 4, 5, 75, 3, 1, 3, 5, 9};
            for (int i = 0; i < unsort.length; i++) {
                for (int j = 0; j < unsort.length; j ++) {
                    if (unsort[i] <= unsort[j]) {
                        int tmp = unsort[i];
                        unsort[i] = unsort[j];
                        unsort[j] = tmp;
                    }
                }
            }
            for (int i = 0; i < unsort.length; i++) {
                System.out.print(unsort[i] + " ");
            }
        }
}
