package com.iceking.apptest;

import com.iceking.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by I on 2015/8/16.
 */
public class AppTestSuit {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext annoContext = new AnnotationConfigApplicationContext(AppConfig.class);
        String[] beans = annoContext.getBeanDefinitionNames();
        for (String bean : beans) {
            System.out.println(bean);
        }
    }

}
