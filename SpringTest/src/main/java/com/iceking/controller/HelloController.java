package com.iceking.controller;

import com.iceking.service.DisplayService;
import com.iceking.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by I on 2015/8/15.
 */
@Controller
public class HelloController {

    @Autowired
    private DisplayService helloService;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @RequestMapping("hi")
    @ResponseBody
    public String hi() {
        return "Hello, world.";
    }

    @RequestMapping
    @ResponseBody
    public String welcome() {
/*        String[] normalBeans = applicationContext.getBeanDefinitionNames();

        for (String bean : normalBeans) {
            System.out.println(bean);
        }

        System.out.println("_______________________________");

        String[] webbeans = webApplicationContext.getBeanDefinitionNames();
        for (String bean : webbeans) {
            System.out.println(bean);
        }*/

        return helloService.welcome();
    }

    @RequestMapping("/xuser/list")
    public String userList(Model model) {
        model.addAttribute("nbUsers", 13);
        return "user/list";
    }

    @RequestMapping("/xuser/{id}/{field}")
    public String showUserField(Model model, @PathVariable("id") Long id,
                              @PathVariable("field") String field) {
        model.addAttribute("newid", id * 2);
        model.addAttribute("newfield", field +  " hehe");
        return "user/list";
    }
}
