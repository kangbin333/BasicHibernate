package com.iceking.controller;

import com.iceking.dao.CategoryDao;
import com.iceking.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by I on 2015/9/22.
 */
@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryDao CategroyDao;

    @RequestMapping("/list")
    public void categoryList(Model model) {
        List<Category> catelist = CategroyDao.getAll();
        model.addAttribute("catelist", catelist);
    }

    @RequestMapping("/add")
    public String categoryAdd(Model model) {
        Category category = new Category();
        model.addAttribute("category", category);
        return "category/addCategory";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String categoryAddSubmit(Category category) {
        CategroyDao.add(category);
        return "redirect:/category/list";
    }

    @RequestMapping("/del/{id}")
    public String categoryDel(@PathVariable long id) {
        CategroyDao.delete(id);
        return "redirect:/category/list";
    }

    @RequestMapping("/update/{id}")
    public String categoryUpdate(Model model, @PathVariable long id) {
        Category category = CategroyDao.getCategoryById(id);
        model.addAttribute("category", category);
        return "category/updateCategory";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String categoryUpdateSubmit(Category category) {
        CategroyDao.update(category);
        return "redirect:/category/list";
    }
}
