package com.iceking.controller;

import com.iceking.dao.UserDao;
import com.iceking.model.Country;
import com.iceking.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by I on 2015/8/16.
 */
@Component
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @RequestMapping("/list")
    public void userList(Model model) {
        List<User> userlist = userDao.list();
        model.addAttribute("userlist", userlist);
    }

    @RequestMapping("/del/{id}")
    public String delUser(@PathVariable long id) {
        userDao.delete(id);
        return "redirect:/user/list";
    }

    @RequestMapping("/addUser")
    public String addUser(Model model) {
        User user = new User();
        model.addAttribute("user", user);

        return "user/addUser";
    }

    /*
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
     public String addUserSubmit(HttpServletRequest request) {
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setAge(Integer.parseInt(request.getParameter("age")));
        userDao.addUserByHiber(user);
        return "redirect:list"; //没有redirect的意味着直接转向到相应的jsp页面，有redirect意味着先通过Spring的controller在转向到相应的jsp页面
    }
    */

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUserSubmit(@Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            return "user/addUser";
        } else {
            userDao.add(user);
            return "redirect:/user/list"; //没有redirect的意味着直接转向到相应的jsp页面，有redirect意味着先通过Spring的controller在转向到相应的jsp页面
        }

    }

    @RequestMapping("/updateUser/{id}")
    public String updateUser(@PathVariable long id, Model model) {
        User user = userDao.getUserById(id);
        model.addAttribute("user", user);
        return "user/updateUser";
    }

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.POST)
    public String updateUserSubmit(@Valid User user, @PathVariable long id, BindingResult result) {
        if (result.hasErrors()) {
            return "user/updateUser";
        } else {
            userDao.update(user);
            return "redirect:/user/list"; //没有redirect的意味着直接转向到相应的jsp页面，有redirect意味着先通过Spring的controller在转向到相应的jsp页面
        }
    }

    @RequestMapping("/addDefaultUser")
    public String addDfaultUser() {
        return "user/addUser";
    }

    @RequestMapping("/addUserByArg")
    public String addUserByArg(@RequestParam String name, @RequestParam int age) {
        User user = new User();
        user.setName(name);
        user.setAge(age);
        userDao.add(user);
        return "user/list";
    }

    @RequestMapping("/add")
    public void addUser(User user) {
        user.setName("Shi");
        user.setAge(28);
        userDao.add(user);
    }

    @RequestMapping("/{id}")
    public String findById(@PathVariable long id) {
        User user = userDao.getUserById(id);
        System.out.println(user);
        return null;
    }

    @ModelAttribute("defaultUser")
    public User defaultUser() {
        User user = new User();
        user.setName("NeverLost");
        user.setAge(100);
        user.setSex("male");
        return user;
    }

    @ModelAttribute("sex")
    public Map<String, String> sex() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("male", "男");
        map.put("female", "女");
        return map;
    }

    @ModelAttribute("colors")
    public List<String> colors() {
        List<String> colors = new ArrayList<String>();
        colors.add("black");
        colors.add("white");
        colors.add("yellow");
        return colors;
    }

    @ModelAttribute("countries")
    public List<Country> countries() {
        List<Country> countries = new ArrayList<Country>();
        countries.add(new Country("chs", "中华人民共和国"));
        countries.add(new Country("us", "美国"));
        return countries;
    }

    @ModelAttribute("languages")
    public Map<String, String> languages() {
        Map<String ,String> list = new HashMap<String, String>();
        list.put("en", "English");
        list.put("fr", "France");
        return list;
    }

    /*
    @ModelAttribute("user")
    public User fuck() {
        return new User();
    }
    */
}
