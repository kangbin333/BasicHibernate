package com.iceking.dao;

import com.iceking.model.User;

import java.util.List;

/**
 * Created by I on 2015/8/21.
 */
public interface UserDao {

    void add(User user);
    void update(User user);
    void delete(long id);

    List<User> list();
    User getUserById(long id);

}
