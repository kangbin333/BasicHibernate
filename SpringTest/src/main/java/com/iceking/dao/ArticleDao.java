package com.iceking.dao;

import com.iceking.model.Article;

import java.util.List;

/**
 * Created by I on 2015/9/23.
 */
public interface ArticleDao {

    void add(Article article);
    void update(Article article);
    void delete(long id);

    List<Article> getAll();
    Article getArticleById(long id);

}
