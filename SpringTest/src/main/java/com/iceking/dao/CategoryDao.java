package com.iceking.dao;

import com.iceking.model.Category;
import java.util.List;

public interface CategoryDao {
    void add(Category category);
    void delete(long id);
    void update(Category category);

    Category getCategoryById(long id);
    List<Category> getAll();
}