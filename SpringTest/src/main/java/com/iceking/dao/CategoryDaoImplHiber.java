package com.iceking.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.transaction.Transactional;
import com.iceking.model.Category;

@Repository
public class CategoryDaoImplHiber implements CategoryDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    public void add(Category category) {
        Session session = sessionFactory.getCurrentSession();
        session.save(category);
    }

    @Transactional
    public void delete(long id) {
        Session session = sessionFactory.getCurrentSession();
        Category category = (Category) session.load(Category.class, id);
        session.delete(category);
    }

    @Transactional
    public void update(Category category) {
        Session session = sessionFactory.getCurrentSession();
        session.update(category);
    }

    @Transactional
    public Category getCategoryById(long id) {
        Session session = sessionFactory.getCurrentSession();
        return (Category) session.load(Category.class, id);
    }

    @Transactional
    public List<Category> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List<Category> list = session.createQuery("from Category").list();
        return list;
    }
}