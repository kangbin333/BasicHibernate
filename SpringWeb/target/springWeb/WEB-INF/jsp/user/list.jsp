<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title></title>
</head>
<body>
    <p>There are ${nbUsers} users!!!</p>

    <p>There are <%=request.getAttribute("nbUsers")%> users!!!</p>

    <p>There is ${newid} in ${newfield}</p>

    <a href="addUser">添加用户</a>
    <c:forEach items="${userlist}" var="user">
        <p><c:out value="${user}" /><a href="del/${user.id}">删除</a><a href="updateUser/${user.id}">更新</a> </p>
    </c:forEach>
</body>
</html>
