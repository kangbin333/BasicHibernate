<%--
  Created by IntelliJ IDEA.
  User: I
  Date: 2015/9/23
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title></title>
</head>
<body>
 <form:form method="post" modelAttribute="article">
   <form:input path="title"/>
   <form:input path="sendPerson"/>
   <form:input path="contents"/>
   <form:checkboxes path="categories" items="${categories}"/>
   <input type="submit" value="添加文章"/>
 </form:form>
</body>
</html>
