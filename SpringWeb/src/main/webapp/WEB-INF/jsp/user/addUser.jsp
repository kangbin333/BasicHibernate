<%--
  Created by IntelliJ IDEA.
  User: I
  Date: 2015/8/18
  Time: 2:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>添加用户</title>
    <%--    <style>
            .error {
                color: #ff0000;
            }
        </style>--%>
</head>
<body>
<%--    <form method="post">
        <input type="text" name="name" />
        <input type="text" name="age" />
        <input type="submit" />
    </form>--%>
<form:form method="post" modelAttribute="user">
    <form:input path="name"/>
    <form:errors path="name" cssClass="error"/>
    <form:input path="age"/>
    <form:errors path="age" cssClass="error"/>
    <form:select path="sex" items="${sex}"/>
    <form:select path="color" items="${colors}"/>
    <form:select path="country" items="${countries}" itemLabel="fullname" itemValue="shortname"/>
    婚否<form:checkbox path="married"/>
    <%--语言<form:checkboxes path="languages" items="${languages}" />--%>
    语言<form:radiobuttons path="languages" items="${languages}"/>
    <input type="submit" value="添加"/>
</form:form>
</body>
</html>
