package com.iceking.testhiber;

/**
 * Created by I on 2015/7/16.
 */
public class TestClass {

    public TestClass() {
        System.out.println("TC");
    }
    public static int add(int i, int j) {
        return  i + j;
    }

    public class Pig {
        
        public Pig() {
            System.out.println("Pig");
        }

    }

    private class Dog {
        public Dog() {
            System.out.println("Dog");
        }
    }

    protected class Cat {
        public Cat() {
            System.out.println("Cat");
        }
    }

}
class SubClass {
    SubClass() {
        System.out.println("SB");
    }
}