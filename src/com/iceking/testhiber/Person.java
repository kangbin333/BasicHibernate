package com.iceking.testhiber;

import javax.annotation.Generated;
import javax.persistence.*;

/**
 * Created by I on 2015/7/14.
 */
public class Person {

    private int id;
    private String name;
    private Address address;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
