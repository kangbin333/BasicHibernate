package com.iceking.testhiber;

import com.sun.org.apache.xalan.internal.xsltc.dom.AdaptiveResultTreeImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by I on 2015/7/14.
 */
public class TestHiberMain {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();

        Address address = new Address();
        address.setPlace("TaiYuan");

        Address address1 = new Address();
        address1.setPlace("BeiJin");

        Person p = new Person();
        p.setName("King");
        p.setAddress(address);

        Person p1 = new Person();
        p1.setName("Wang");
        p1.setAddress(address);

        session.save(address);
        session.save(address1);
        session.save(p);
        session.save(p1);

        tx.commit();

        List<Person> addlist = session.createQuery("from Person p where p.address = :addid")
                .setInteger("addid", 1)
                .list();

        BigInteger pp = (BigInteger) session.createSQLQuery("select count(*) from Person p where p.addressid = ?")
                .setInteger(0, 1)
                .uniqueResult();

        System.out.println(pp.intValue());
        for (Person a : addlist) {
            System.out.println(a.getName());
        }

        session.close();

    }

}
